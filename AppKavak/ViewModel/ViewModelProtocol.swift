//
//  ViewModelProtocol.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

@objc protocol ViewModelProtocol {
    @objc optional func fetchDataSuccess()
    @objc optional func fetchDataFailure(with error: APIError)
    @objc optional func updateResultsController()
}
