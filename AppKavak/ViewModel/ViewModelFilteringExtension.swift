//
//  ViewModelFilteringExtension.swift
//  AppKavak
//
//  Created by ARIEL DÍAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation
import NaturalLanguage

// MARK: SearchEngineProtocol
extension GnomeViewModel: SearchEngineProtocol {
    func didPublish(streamData: String) {
        filteredGnomes = []
        setSpellCheckingFeatures(for: streamData, dominantLanguage: .english)
        filterAllData(with: streamData)
    }

    func setSpellCheckingFeatures(for string: String, dominantLanguage: NLLanguage) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            // initialize UITextChecker, nsString, stringRange
            let textChecker = UITextChecker()
            let nsString = NSString(string: string)
            let stringRange = NSRange(location: 0, length: nsString.length)
            var offset = 0
            repeat {
                let wordRange = textChecker.rangeOfMisspelledWord(
                    in: string,
                    range: stringRange,
                    startingAt: offset,
                    wrap: false,
                    language: dominantLanguage.rawValue
                )
                guard wordRange.location != NSNotFound else { break }
                debugPrint(nsString.substring(with: wordRange))
                if let guesses = textChecker.guesses(
                    forWordRange: wordRange,
                    in: string,
                    language: dominantLanguage.rawValue) {
                    debugPrint("setSuggestions: \(guesses), for language: [ \(dominantLanguage.rawValue) ]")
                    self?.setSuggestions(for: guesses)
                }
                offset = wordRange.upperBound
            } while true
        }
    }

    func setSuggestions(for guesses: [String]) {
        debugPrint(guesses)
        var filteredGnomes: [Gnome]!
        guesses.forEach { guess in
            filteredGnomes = dataSource?.gnomes?.filter {
                $0.name?.prepareForFilter().contains(guess.prepareForFilter()) ?? false
                || guess.prepareForFilter().contains($0.name?.prepareForFilter() ?? "notComparableString")
            }
            self.filteredGnomes.append(contentsOf: filteredGnomes)
        }
        self.filteredGnomes = self.filteredGnomes.uniqueElements
        delegates?.forEach {
            $0.updateResultsController?()
        }
    }

    func filterAllData(with streamData: String) {
        let guesses = streamData.components(separatedBy: " ")
        DispatchQueue.global(qos: .background).async { [weak self] in
            var filteredGnomes: [Gnome]!
            guesses.forEach { guess in
                filteredGnomes = self?.dataSource?.gnomes?.filter {
                    $0.name?.prepareForFilter().contains(guess.prepareForFilter()) ?? false
                    || guess.prepareForFilter().contains($0.name?.prepareForFilter() ?? "notComparableString")
                    || $0.professions.reduce("", { $0 + $1}).contains(guess.prepareForFilter())
                    || guess.prepareForFilter().contains($0.professions.reduce("", { $0 + $1}).prepareForFilter())
                }
                self?.filteredGnomes.append(contentsOf: filteredGnomes)
            }
            self?.filteredGnomes = self?.filteredGnomes?.uniqueElements
            self?.delegates?.forEach {
                $0.updateResultsController?()
            }
        }
    }
}
