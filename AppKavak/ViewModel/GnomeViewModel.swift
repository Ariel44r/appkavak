//
//  HomeViewModel.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 18/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit
import Combine

class GnomeViewModel: WrapperLayer {
    private let url: String = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json"
    var dataSource: DataSource!
    var delegates: [ViewModelProtocol]?
    private let classificationService = ClassificationService()
    @Published var filteredGnomes: [Gnome]! = []
    var cancellable: AnyCancellable?

    override init() {
        super.init()
        self.dataSource = DataSource()
        self.setPublishedVars()
        self.delegates = []
        SearchEngine.shared.delegate = self
    }

    func fetchData() {
        if let gnomes = dataSource.gnomes, !gnomes.isEmpty {
            delegates?.forEach {
                $0.fetchDataSuccess?()
            }
        } else {
            fetchDataFromServer(completion: nil)
        }
    }

    @objc func fetchDataFromServer(completion: Completion?) {
        sendRequest(
            with: [
                .endPoint(url),
                .mimeType(.json),
                .method(.GET)],
            onSuccess: { [weak self] (response: DataResponse) in
                guard response.brastlewark != nil else { return }
                self?.dataSource?.deleteRecords()
                CoreDataManager.shared.saveContext()
                self?.delegates?.forEach {
                    $0.fetchDataSuccess?()
                }
                self?.dataSource?.setGnomeArrays()
                self?.dataSource?.setProfessionsArrays(completion)
        }, onError: { [weak self] error in
            self?.delegates?.forEach {
                $0.fetchDataFailure?(with: error)
            }
            debugPrint(error.domain)
            completion?()
        })
    }

    @objc func fetchDataTLS(completion: Completion?) {
        sendRequest(with: [.endPoint("http://192.168.100.28:80/feature0"), .method(.GET), .mimeType(.json)],
        onSuccess: { (response: [String]) in
            debugPrint(response)
            completion?()
        }, onError: { error in
            debugPrint(error)
        })
    }

    func setPublishedVars() {
        cancellable = filteredGnomes?.publisher.sink(receiveValue: { gnome in
            debugPrint(gnome.name ?? "")
        })
    }

    func setCurrentGnome(at indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            dataSource?.currentGnome = dataSource?.popularGnomes?[indexPath.item]
        case 1:
            dataSource?.currentGnome = dataSource?.hardWorkerGnomes?[indexPath.item]
        default:
            dataSource?.currentGnome = dataSource?.randomGnomes?[indexPath.item]
        }
        setTimestampForCurrentGnome()
    }

    func setCurrentFilteredGnome(at indexPath: IndexPath) {
        dataSource?.currentGnome = filteredGnomes[indexPath.row]
        setTimestampForCurrentGnome()
    }

    func setCurrentSuggestedGnome(at indexPath: IndexPath) {
        dataSource?.currentGnome = dataSource?.mostViewedGnomes?[indexPath.row]
        setTimestampForCurrentGnome()
    }

    func setCurrentGnomeFromCategories(at indexPath: IndexPath) {
        dataSource?.currentGnome = dataSource?.professionsGnomes?[indexPath.section][indexPath.item]
        setTimestampForCurrentGnome()
    }

    func setTimestampForCurrentGnome() {
        let timeStamp = "\(Date().timeIntervalSinceReferenceDate)"
        dataSource?.currentGnome?.lastView = timeStamp
        dataSource?.currentGnome?.views = (dataSource?.currentGnome?.views ?? 0) + 1
        CoreDataManager.shared.saveContext()
    }

    func getGenderColor(name: String?) -> UIColor? {
        guard let name = name?.components(separatedBy: " ").first,
        let result = try? classificationService.predictGender(from: name)
        else { return nil }
        return result.gender.string == "Male" ? .systemBlue : .systemPink
    }
}

// MARK: ComputedVars
extension GnomeViewModel {
    func numberOfItemsInHome(with section: Int) -> Int {
        switch section {
        case 0:     return dataSource?.popularGnomes?.count ?? 0
        case 1:     return dataSource?.hardWorkerGnomes?.count ?? 0
        default:    return dataSource?.randomGnomes?.count ?? 0
        }
    }
}

// MARK: FillUIViews
extension GnomeViewModel {
    func set(view: GnomeCollectionViewCell?, indexPath: IndexPath) {
        guard let cell = view else { return }
        cell.fxView?.isHidden = indexPath.section != 0
        switch indexPath.section {
        case 0:
            if let gnome = dataSource?.popularGnomes?[indexPath.item] {
                if let url = gnome.thumbnail { cell.imageViewFull?.getImage(from: url) }
                let gnomeProfessions = gnome.professions
                cell.buttons?.first?.setTitle("\(gnomeProfessions.count) Prof", for: .normal)
                cell.buttons?.first?.isHidden = gnomeProfessions.isEmpty
                let gnomeFriends = gnome.friends
                cell.buttons?.last?.setTitle("\(gnomeFriends.count) Frie", for: .normal)
                cell.buttons?.last?.isHidden = gnomeFriends.isEmpty
                cell.labels?.first?.text = gnome.name
                cell.labels?[1].text = " > Weight: \(gnome.weight)\n > Height: \(gnome.height)"
                cell.labels?.last?.text = " > Hair color: \(gnome.hairColor ?? "")"
                cell.groupButton?.setImage(UIImage(systemName: "person.fill"), for: .normal)
                cell.groupButton?.tintColor = getGenderColor(name: gnome.name)
            }
        case 1:
            if let gnome = dataSource?.hardWorkerGnomes?[indexPath.item] {
                if let url = gnome.thumbnail { cell.imageViewFull?.getImage(from: url) }
            }
        default:
            if let gnome = dataSource?.randomGnomes?[indexPath.item] {
                if let url = gnome.thumbnail { cell.imageViewFull?.getImage(from: url) }
            }
        }
    }

    func set(with view: GnomeCollectionViewCell?, indexPath: IndexPath) {
        if let cell = view, let gnome = dataSource?.professionsGnomes?[indexPath.section][indexPath.item] {
            cell.fxView?.isHidden = !(indexPath.section == 3 || indexPath.section == 1)
            if let url = gnome.thumbnail { cell.imageViewFull?.getImage(from: url) }
            let gnomeProfessions = gnome.professions
            cell.buttons?.first?.setTitle("\(gnomeProfessions.count) Prof", for: .normal)
            cell.buttons?.first?.isHidden = gnomeProfessions.isEmpty
            let gnomeFriends = gnome.friends
            cell.buttons?.last?.setTitle("\(gnomeFriends.count) Frie", for: .normal)
            cell.buttons?.last?.isHidden = gnomeFriends.isEmpty
            cell.labels?.first?.text = gnome.name
            cell.labels?[1].text = " > Weight: \(gnome.weight)\n > Height: \(gnome.height)"
            cell.labels?.last?.text = " > Hair color: \(gnome.hairColor ?? "")"
            cell.groupButton?.setImage(UIImage(systemName: "person.fill"), for: .normal)
            cell.groupButton?.tintColor = getGenderColor(name: gnome.name)
            if indexPath.section == 1 {
                cell.childContainerViews?.first?.isHidden = true
                cell.childContainerViews?.last?.isHidden = true
                cell.labels?[1].isHidden = true
                cell.labels?[2].isHidden = true
            } else if indexPath.section == 3 {
                cell.childContainerViews?.last?.isHidden = true
            }
        }
    }

    func set(with view: CategoryCollectionViewCell?, indexPath: IndexPath) {
        if let cell = view, let gnome = dataSource?.professionsGnomes?[indexPath.section][indexPath.item] {
            cell.imageViewGender.tintColor = getGenderColor(name: gnome.name)
            cell.imageViewGender.image = UIImage(systemName: "person.fill")
            cell.imageViewThumbnail.getImage(from: gnome.thumbnail ?? "")
            cell.labels?.first?.text = gnome.name
            cell.labels?.last?.text = " > Weight: \(gnome.weight)\n > Height: \(gnome.height)"
        }
    }

    func set(view: TopImageTableViewCell?) {
        guard let cell = view,
        let currentGnome = dataSource?.currentGnome,
        let thumbnail = currentGnome.thumbnail
        else { return }
        cell.imageViewFull?.getImage(from: thumbnail)
        cell.imageViewBg?.getImage(from: thumbnail)
    }

    func set(view: DetailAttributeTableViewCell?, indexPath: IndexPath) {
        guard let cell = view, let currentGnome = dataSource?.currentGnome else { return }
        switch indexPath.section {
//        case 0:
//            cell.imageView?.image = UIImage(systemName: "person.fill")
//            cell.textLabel?.text = "    [Name: \(currentGnome.name ?? "")]"
        case 0:
            cell.imageView?.image = UIImage(systemName: "w.circle.fill")
            cell.textLabel?.text = "    [Weight: \(currentGnome.weight) Lb]"
        case 1:
            cell.imageView?.image = UIImage(systemName: "h.circle.fill")
            cell.textLabel?.text = "    [Height: \(currentGnome.height) cm]"
        case 2:
            cell.imageView?.image = UIImage(systemName: "briefcase.fill")
            cell.textLabel?.text = "    [Professions: \(currentGnome.professions.count)]"
        case 3:
            cell.imageView?.image = UIImage(systemName: "person.3.fill")
            cell.textLabel?.text = "    [Friends: \(currentGnome.friends.count)]"
        case 4:
            cell.imageView?.image = UIImage(systemName: "h.circle.fill")
            cell.textLabel?.text = "    [Hair Color: \(currentGnome.hairColor ?? "")]"
        case 5:
            cell.imageView?.image = UIImage(systemName: "person.fill")
            guard let genderColor = getGenderColor(name: currentGnome.name ?? "") else { return }
            cell.textLabel?.text = "    [Gender: \(genderColor == .systemBlue ? "Male" : "Female")]"
            cell.imageView?.tintColor = genderColor
        case 6:
            cell.imageView?.image = UIImage(systemName: "calendar")
            cell.textLabel?.text = "    [Age: \(currentGnome.age) years old]"
        default: break
        }
    }

    func set(view: UITableViewCell?, indexPath: IndexPath) {
        guard let cell = view, let currentGnome = dataSource?.currentGnome else { return }
        let currentObjectString = indexPath.item == 0
            ? currentGnome.professions[indexPath.section]
            : currentGnome.friends[indexPath.section]
        cell.textLabel?.text = currentObjectString
    }

    func set(view: ResultsTableViewCell?, indexPath: IndexPath) {
        guard let cell = view, indexPath.row < filteredGnomes.count,
        let gnome = filteredGnomes?[indexPath.row]
        else { return }
        cell.imageViewGender.tintColor = getGenderColor(name: gnome.name)
        cell.imageViewGender.image = UIImage(systemName: "person.fill")
        cell.imageViewThumbnail.getImage(from: gnome.thumbnail ?? "")
        cell.labels?.first?.text = gnome.name
        cell.labels?.last?.text = " > Weight: \(gnome.weight)\n > Height: \(gnome.height)"
    }

    func setSuggestion(view: ResultsTableViewCell?, indexPath: IndexPath) {
        guard let mostViewedGnomes = dataSource?.mostViewedGnomes,
        let cell = view,
        indexPath.row < mostViewedGnomes.count
        else { return }
        let gnome = mostViewedGnomes[indexPath.row]
        cell.imageViewGender.tintColor = getGenderColor(name: gnome.name)
        cell.imageViewGender.image = UIImage(systemName: "person.fill")
        cell.imageViewThumbnail.getImage(from: gnome.thumbnail ?? "")
        cell.labels?.first?.text = gnome.name
        cell.labels?.last?.text = " > Weight: \(gnome.weight)\n > Height: \(gnome.height)"
    }
}
