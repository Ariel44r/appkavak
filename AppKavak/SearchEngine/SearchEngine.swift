//
//  SearchEngine.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit
import Combine

@objc protocol SearchEngineProtocol {
    @objc optional func didPublish(streamData: String)
}

class SearchEngine: NSObject {
    public static let shared: SearchEngine = SearchEngine()
    var searchController: UISearchController!
    var resultsTableViewController: ResultsTableViewController!
    var cancellable: AnyCancellable?
    var gnomeViewModel: GnomeViewModel!
    weak var delegate: SearchEngineProtocol?

    private override init() {
        super.init()
        setSearchController()
    }

    func setSearchController() {
        resultsTableViewController = ResultsTableViewController(style: .plain)
        searchController = UISearchController(searchResultsController: resultsTableViewController)
        resultsTableViewController.searchBar = searchController?.searchBar
        delegate = gnomeViewModel
        searchController?.searchBar.delegate = self
        searchController?.searchBar.tintColor = .master
        searchController?.searchBar.placeholder = "Find Gnomes, Professions, ..."
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.searchResultsUpdater = self
        setDataStream()
    }

    func setDataStream() {
        let notification = UISearchTextField.textDidChangeNotification
        let object = searchController.searchBar.searchTextField
        let schedul = RunLoop.main
        let notificationCenter = NotificationCenter.default
        let publisher = notificationCenter.publisher(for: notification, object: object)
        let publisherDebounce = publisher.debounce(for: .milliseconds(200), scheduler: schedul)
        cancellable = publisherDebounce.sink(receiveValue: { [weak self] notification in
            guard let searchTextField = notification.object as? UISearchTextField,
            let text = searchTextField.text, text != " "
            else { return }
            self?.delegate?.didPublish?(streamData: text)
        })
    }

}

extension SearchEngine: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchResultsController?.view.isHidden = false
    }
}

// MARK: UISearchBarDelegate
extension SearchEngine: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        debugPrint("searchBarTextDidBeginEditing")
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("searchBarSearchButtonClicked")
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("searchBarCancelButtonClicked")
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        debugPrint("searchBarTextDidChange")
    }
}
