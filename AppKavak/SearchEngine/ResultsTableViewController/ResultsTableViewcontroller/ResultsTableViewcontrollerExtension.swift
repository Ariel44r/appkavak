//
//  ResultsTableViewcontrollerExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

enum TableViewState {
    case suggestions
    case results

    var titleHeaders: [String] {
        switch self {
        case .suggestions:
            return ["Suggestions:"]
        case .results:
            return ["Gnomes:"]
        }
    }
    var numberOfSections: Int {
        switch self {
        case .suggestions:
            return 1
        case .results:
            return 1
        }
    }
}

extension ResultsTableViewController {
    var tableViewState: TableViewState { (searchBar?.text ?? "")  == "" ? .suggestions : .results }

    func numberOfRows(section: Int) -> Int {
        switch self.tableViewState {
        case .suggestions:
            return gnomeViewModel?.dataSource?.mostViewedGnomes?.count ?? 0
        case .results:
            return gnomeViewModel?.filteredGnomes?.count ?? 0
        }
    }

    func titleMeanWhile(for indexPath: IndexPath) -> String {
        switch tableViewState {
        case .suggestions:
            return "Suggestions"
        case .results:
            return "Gnomes"
        }
    }
}
