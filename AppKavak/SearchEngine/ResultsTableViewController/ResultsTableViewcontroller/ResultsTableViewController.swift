//
//  ResultsTableViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 18/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

@objc protocol SearchResultsProtocol {
    @objc optional func showDetailObject()
}

private let reuseIdentifier: String = "ResultsTableViewCell"
class ResultsTableViewController: BaseTableViewController {
    var searchBar: UISearchBar!
    weak var delegate: SearchResultsProtocol?
    override var gnomeViewModel: GnomeViewModel! { SearchEngine.shared.gnomeViewModel }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        tableView?.keyboardDismissMode = .onDrag
        gnomeViewModel?.delegates?.append(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewState.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows(section: section)
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        weak var view: HeaderView! = UIView.fromNib()
        view?.label?.text = tableViewState.titleHeaders[section]
        return view
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? ResultsTableViewCell
        else { fatalError("the cell \(ResultsTableViewCell.self)") }
        switch tableViewState {
        case .suggestions:
            gnomeViewModel?.setSuggestion(view: cell, indexPath: indexPath)
        case .results:
            gnomeViewModel?.set(view: cell, indexPath: indexPath)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableViewState {
        case .results:
            gnomeViewModel?.setCurrentFilteredGnome(at: indexPath)
        default:
            gnomeViewModel?.setCurrentSuggestedGnome(at: indexPath)
        }
        (parent as? UISearchController)?.dismiss(animated: true, completion: { [weak self] in
            self?.delegate?.showDetailObject?()
        })
    }

}

// MARK: ViewModelProtocol
extension ResultsTableViewController: ViewModelProtocol {
    func updateResultsController() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView?.reloadData()
        }
    }
}
