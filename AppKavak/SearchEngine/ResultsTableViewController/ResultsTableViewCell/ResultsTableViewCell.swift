//
//  ResultsTableViewCell.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewThumbnail: UIImageView!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var imageViewGender: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        imageViewThumbnail?.makeViewWith(features: [.roundedView(.full, .master)])
    }

}
