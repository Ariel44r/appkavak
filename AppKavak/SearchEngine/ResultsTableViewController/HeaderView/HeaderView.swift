//
//  HeaderView.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class HeaderView: UICollectionReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var label: UILabel!

}
