//
//  Response.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 18/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

class DataResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case brastlewark="Brastlewark"
    }

    var brastlewark: [Gnome]!

    required init(from decoder: Decoder) throws {
        let values  =   try decoder.container(keyedBy: CodingKeys.self)
        brastlewark =   try? values.decode([Gnome].self, forKey: .brastlewark)
    }
}
