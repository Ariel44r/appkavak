//
//  Gnome+CoreDataClass.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//
//

import Foundation
import CoreData

public class Gnome: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case id="id"
        case name="name"
        case thumbnail="thumbnail"
        case age="age"
        case weight="weight"
        case height="height"
        case hairColor="hair_color"
        case professions="professions"
        case friends="friends"
    }

    required convenience public init(from decoder: Decoder) throws {
        guard let infoKey = CodingUserInfoKey.context,
        let context = decoder.userInfo[infoKey] as? NSManagedObjectContext,
        let entity = NSEntityDescription.entity(forEntityName: "Gnome", in: context)
        else { fatalError("Failed to decode Gnome") }
        let container       =   try decoder.container(keyedBy: CodingKeys.self)
        let id: Int64!              =   try? container.decodeIfPresent(Int64.self, forKey: .id) ?? 0
        let name: String!           =   try? container.decodeIfPresent(String.self, forKey: .name) ?? ""
        let thumbnail: String!      =   try? container.decodeIfPresent(String.self, forKey: .thumbnail) ?? ""
        let age: Int64!             =   try? container.decodeIfPresent(Int64.self, forKey: .age) ?? 0
        let weight: Float!          =   try? container.decodeIfPresent(Float.self, forKey: .weight) ?? 0
        let height: Float!          =   try? container.decodeIfPresent(Float.self, forKey: .height) ?? 0
        let hairColor: String!      =   try? container.decodeIfPresent(String.self, forKey: .hairColor) ?? ""
        let professions: [String]!  =   try? container.decodeIfPresent([String].self, forKey: .professions) ?? []
        let friends: [String]!      =   try? container.decodeIfPresent([String].self, forKey: .friends) ?? []

        self.init(entity: entity, insertInto: context)
        self.id = id
        self.name = name
        self.thumbnail = thumbnail
        self.age = age
        self.weight = weight
        self.height = height
        self.hairColor = hairColor
        self.professions = professions
        self.friends = friends
        self.views = 0
        self.lastView = ""
    }

    public func encode(to encoder: Encoder) throws { }
}

extension CodingUserInfoKey {
   static let context = CodingUserInfoKey(rawValue: "context")
}
