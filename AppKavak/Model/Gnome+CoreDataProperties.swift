//
//  Gnome+CoreDataProperties.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//
//

import Foundation
import CoreData

extension Gnome {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Gnome> {
        return NSFetchRequest<Gnome>(entityName: "Gnome")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var thumbnail: String?
    @NSManaged public var age: Int64
    @NSManaged public var weight: Float
    @NSManaged public var height: Float
    @NSManaged public var hairColor: String?
    @NSManaged public var professions: [String]
    @NSManaged public var friends: [String]
    @NSManaged public var views: Int64
    @NSManaged public var lastView: String?

}
