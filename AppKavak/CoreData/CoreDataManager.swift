//
//  CoreDataManager.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {

    public static let shared = CoreDataManager()

    var context: NSManagedObjectContext!
    private init() {
        context = persistentContainer.viewContext
    }

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            debugPrint(storeDescription.description, error.debugDescription)
        })
        return container
    }()

    func saveContext() {
        if context.hasChanges {
            try? context?.save()
        }
    }

}
