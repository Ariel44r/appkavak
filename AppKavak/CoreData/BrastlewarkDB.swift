//
//  DataSource.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation
import CoreData

class DataSource {
    private let coreDataManager = CoreDataManager.shared
    private var gnomesDB: [Gnome]? { try? coreDataManager.context.fetch(Gnome.fetchRequest()) as? [Gnome] }
    private var popularGnomesDB: [Gnome] {
        guard let mostFriendsGnomes = gnomes?.sorted(by: { $0.friends.count > $1.friends.count }),
        !mostFriendsGnomes.isEmpty
        else { return [] }
        return (0 ..< 10).map { mostFriendsGnomes[$0] }
    }
    private var hardWorkerGnomesDB: [Gnome] {
        guard let mostHardWorkerGnomes = gnomes?.sorted(by: { $0.professions.count > $1.professions.count }),
        !mostHardWorkerGnomes.isEmpty
        else { return [] }
        return (0 ..< 10).map { mostHardWorkerGnomes[$0] }
    }
    private var randomGnomesDB: [Gnome] {
        guard let gnomes = self.gnomes, !gnomes.isEmpty else { return [] }
        return (0 ..< 18).map { _ in
            let randomIndex: Int = .random(in: (0 ..< gnomes.count))
            return gnomes[randomIndex]
        }
    }
    var mostViewedGnomes: [Gnome]? {
        let fetchRequest: NSFetchRequest<Gnome> = Gnome.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "views > %@", "0")
        return try? coreDataManager.context.fetch(fetchRequest).sorted(by: { $0.views > $1.views })
    }

    var currentGnome: Gnome!
    var gnomes: [Gnome]!
    var popularGnomes: [Gnome]!
    var hardWorkerGnomes: [Gnome]!
    var randomGnomes: [Gnome]!
    var professionsGnomes: [[Gnome]]!

    init() {
        setGnomeArrays()
        setProfessionsArrays(nil)
    }

    func setGnomeArrays() {
        gnomes = gnomesDB
        popularGnomes = popularGnomesDB
        hardWorkerGnomes = hardWorkerGnomesDB
        randomGnomes = randomGnomesDB
    }

    func setProfessionsArrays(_ completion: Completion?) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            var professions: [String] = []
            self?.gnomes?.forEach {
                professions.append(contentsOf: $0.professions)
            }
            professions = professions.uniqueElements
            let professionsG = professions.map { profession in
                self?.gnomes?.filter { gnome in
                    gnome.professions.first(where: { $0 == profession }) != nil
                }
            }
            let secureProfessionsG = (professionsG.filter { $0 != nil } as? [[Gnome]]) ?? []
            self?.professionsGnomes = secureProfessionsG.map { gnomeArray in
                return (0 ..< 12).map { gnomeArray[$0] }
            }
            completion?()
        }
    }

    func setGnomePopulation(gnomes: [Gnome]) {
        coreDataManager.saveContext()
    }

    func deleteRecords() {
        let fetchRequest: NSFetchRequest<Gnome> = Gnome.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id >= %@", "0")
        guard let fetchRequestResults = fetchRequest as? NSFetchRequest<NSFetchRequestResult> else { return }
        let request = NSBatchDeleteRequest(fetchRequest: fetchRequestResults)
        guard let _ = try? coreDataManager.context.execute(request),
        let _ = try? coreDataManager.context.save()
        else { return }
    }

}
