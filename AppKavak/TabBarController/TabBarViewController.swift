//
//  TabBarViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class TabBarViewController: BaseTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        selectedIndex = 1
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.foregroundColor: UIColor.gray],
            for: .normal
        )
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.foregroundColor: UIColor.tabBarTitles],
            for: .selected
        )
    }

}
