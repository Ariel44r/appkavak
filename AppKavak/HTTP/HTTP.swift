//
//  HTTPFile.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation
import CoreData

public enum MimeType: String {
    case urlEncoded="application/x-www-form-urlencoded"
    case json="application/json"
}

public enum HTTPMethod: String {
    case GET
    case POST
    case HEAD
    case PUT
    case DELETE
    case PATCH
}

public enum RequestFeature {
    case endPoint(String)
    case mimeType(MimeType)
    case method(HTTPMethod)
    case inputData(Data?)
    case body(JSON)
    case headers([String: String])
    case tokenURL(String)
    case timeOutInterval(TimeInterval)
}

@objc open class WrapperLayer: HTTPLayer {
    open func sendRequest<T: Codable>(
    with features: [RequestFeature],
    onSuccess: @escaping Response<T>,
    onError: @escaping ErrorHandler<APIError>) {
        if let error = URLSession.checkAvailability() {
            onError(error)
        } else {
            DispatchQueue.global(qos: .background).async {
                guard let requestPackage = self.getRequest(with: features) else { onError(.defaultError); return }
                self.sendHTTPRequest(with: requestPackage, onSuccess: onSuccess, onError: onError)
            }
        }
    }
}

@objc open class HTTPLayer: NSObject {
    var tokenURL: String?
    var token: String?// = "RmDrQNl7LrfMfLf326bjaKQPmKl7"
    var authHeader: [String: String]?
    let timeOutInterval: TimeInterval = 20

    func getRequest(with features: [RequestFeature]) -> SMRequestPackage? {
        var requestPackage: SMRequestPackage!
        features.forEach {
            switch $0 {
            case .endPoint(let endPoint):
                guard let url = URL(string: endPoint) else { return }
                requestPackage = SMRequestPackage(request: URLRequest(url: url), environment: .api)
            case .mimeType(let mimeType):
                requestPackage?.request.setValue(mimeType.rawValue, forHTTPHeaderField: "Content-Type")
            case .method(let method):           requestPackage?.request.httpMethod = method.rawValue
            case .inputData(let data):          requestPackage?.request.httpBody = data
            case .body(let json):
                requestPackage?.request.httpBody = try? JSONSerialization.data(
                    withJSONObject: json,
                    options: .prettyPrinted
                )
            case .headers(let headers):         requestPackage?.request.allHTTPHeaderFields = headers
            case .timeOutInterval(let timeOut): requestPackage?.request.timeoutInterval = timeOut
            default: break
            }
        }
        requestPackage?.request.timeoutInterval = timeOutInterval
        return requestPackage
    }
}

extension HTTPLayer {
    func sendHTTPRequest<T: Codable>(
    with package: SMRequestPackage,
    onSuccess: @escaping Response<T>,
    onError: @escaping ErrorHandler<APIError>) {
        let url = package.request.url?.absoluteString ?? ""
        let method = package.request.httpMethod ?? ""
        debugPrint("[🚀] [\(T.self)] [\(method): \(url)]")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        session.asyncRequest(request: package.request, completion: { data, response, _ in
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                debugPrint("\(Emoji.e(statusCode)) [\(T.self)] [\(method): \(url)]")
                let decoder = JSONDecoder()
                decoder.userInfo[CodingUserInfoKey.context!] = CoreDataManager.shared.context
                if let data = data, let object = try? decoder.decode(T.self, from: data) {
                    onSuccess(object)
                } else {
                    onError(.serializedError(with: data, code: statusCode))
                }
            }
        })
    }
}

extension HTTPLayer: URLSessionDelegate {
    public func urlSession(_ session: URLSession,
                           didReceive challenge: URLAuthenticationChallenge,
                           completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                if errSecSuccess == status {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData)
                        let size = CFDataGetLength(serverCertificateData)
                        let cert1 = NSData(bytes: data, length: size)
                        if let file = Bundle.main.path(forResource: "servercert.pem", ofType: "cer") {
                            if let cert2 = NSData(contentsOfFile: file) {
                                if cert1.isEqual(to: cert2 as Data) {
                                    let useCredential = URLSession.AuthChallengeDisposition.useCredential
                                    completionHandler(useCredential, URLCredential(trust: serverTrust))
                                    debugPrint("SUCCESS TO handle SSL pinning")
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        debugPrint("ERROR TO handle SSL pinning")
        // Pinning failed completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
}

extension Data {
    var jsonDic: JSON? { (try? JSONSerialization.jsonObject(with: self, options: .mutableLeaves)) as? JSON }
}
