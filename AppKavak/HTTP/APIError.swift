//
//  APIError.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

open class APIError: NSError {
    var data: Data!
    class var defaultError: APIError {
        return APIError()
    }
    public class func serializedError(with data: Data?, code: Int) -> APIError {
        let error = APIError(domain: "Can´t serialize payload data 🤖", code: code, userInfo: ["data": data ?? ""])
        error.data = data
        return error
    }
}
