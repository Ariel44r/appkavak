//
//  Wrappable.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

public typealias JSON = [String: Any]
public typealias JSONData = Data
public typealias JSONString = String
public typealias Completion = () -> Void
public typealias CompletionHandler<T> = (T) -> Void
public typealias Response<T: Codable> = (T) -> Void
public typealias ErrorHandler<T: NSError> = CompletionHandler<T>

extension Encodable {
    func encode<T>() -> T? {
        if let optionalData = try? JSONEncoder().encode(self) as? T,
            let data = optionalData as T? {
            return data
        } else if let optionalData = try? JSONEncoder().encode(self) as JSONData?,
            let data = optionalData as JSONData?,
            let string = JSONString(data: data, encoding: .utf8) as? T {
            return string
        } else if let optionalData = try? JSONEncoder().encode(self) as JSONData?,
            let data = optionalData as JSONData?,
            let jsonDictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? T {
            return jsonDictionary
        } else {
            return nil
        }
    }
}

public extension Sequence where Element: Codable {
    func encode() -> [JSON] {
        var elements: [JSON] = []
        self.forEach {
            if let element: JSON = $0.encode() {
                elements.append(element)
            }
        }
        return elements
    }
}
