//
//  RequestPAckage.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

@objc class SMRequestPackage: NSObject {
    var request: URLRequest
    let environment: Environment
    init(request: URLRequest, environment: Environment) {
        self.request = request
        self.environment = environment
    }
}

@objc open class HTTPResponse: NSObject, Codable { }

enum Environment {
    case api
}

class Emoji: NSObject {
    @objc class func e(_ code: Int) -> String {
        var string: String = " [status code \(code)]"
        switch code {
        case (200 ... 201):
            string = "[✅]" + string
        case 401:
            string = "[🔄]" + string
        case 404:
            string = "[4️⃣0️⃣4️⃣] not found!" + string
        case 500:
            string = "[💣]" + string
        default: break
        }
        return string
    }
    class var bot: String { "🤖" }
}
