//
//  Coordinator.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator]! { get set }
    var mainCoordinator: Coordinator! { get }

    func start()
    func finish()
}

protocol NavigationCoordinator: Coordinator {
    func push<T: UIViewController>(type: T.Type, storyboardName: String?)
    func pop()
}
protocol TabBarCoordinator: Coordinator { }

extension Coordinator {
    func getViewController<T: UIViewController>(_ type: T.Type, storyboardName: String?) -> T? {
        let nib: T! = T(nibName: String(describing: T.self), bundle: nil)
        if nib != nil {
            return nib
        } else if let storyboardName = storyboardName {
            let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
            let identifier: String = String(describing: T.self)
            let controller = storyboard.instantiateViewController(withIdentifier: identifier)
            guard let viewController = controller as? T else { return nil }
            return viewController
        } else {
            return nil
        }
    }

    func start() {
        preconditionFailure("This method needs to be overriden by concrete subclass.")
    }

    func finish() {
        preconditionFailure("This method needs to be overriden by concrete subclass.")
    }

    mutating func addChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func removeChildCoordinator(_ coordinator: Coordinator) {
        preconditionFailure("This method needs to be overriden by concrete subclass.")
    }

    mutating func removeAllChildCoordinatorsWith<T>(type: T.Type) {
        childCoordinators = childCoordinators.filter { $0 is T  == false }
    }

    mutating func removeAllChildCoordinators() {
        childCoordinators.removeAll()
    }

}
