//
//  GnomeCollectionViewCell.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 18/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class GnomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageViewFull: UIImageView!
    @IBOutlet weak var fxView: UIVisualEffectView!
    @IBOutlet weak var groupButton: IndexButton!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet var buttons: [IndexButton]!
    @IBOutlet var childContainerViews: [UIView]!

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView?.makeViewWith(features: [.bordered(.lightGray, 0.05), .shadow, .rounded])
        fxView?.makeViewWith(features: [.rounded])
        buttons?.forEach { $0.makeViewWith(features: [.rounded]) }
    }

}
