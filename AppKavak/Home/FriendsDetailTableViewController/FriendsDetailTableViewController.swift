//
//  FriendsDetailTableViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

private let reuseIdentifier: String = "UITableViewCell"
class FriendsDetailTableViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Friends detail"
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return gnomeViewModel?.dataSource?.currentGnome?.friends.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        gnomeViewModel?.set(view: cell, indexPath: IndexPath(item: 1, section: indexPath.section))
        return cell
    }

}
