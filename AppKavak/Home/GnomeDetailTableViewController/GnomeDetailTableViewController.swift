//
//  GnomeDetailTableViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

private let reuseIdentifier1: String = "DetailAttributeTableViewCell"
class GnomeDetailTableViewController: BaseTableViewController {
    var parallaxView: TopImageTableViewCell!
    var offset: CGPoint!
    var isActiveViewer: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "\(gnomeViewModel?.dataSource?.currentGnome?.name ?? "")"
        setParallax()
        tableView?.register(UINib(nibName: reuseIdentifier1, bundle: nil), forCellReuseIdentifier: reuseIdentifier1)
        isActiveViewer = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let offset = self.offset else { return }
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.tableView?.setContentOffset(offset, animated: false)
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.alpha = 1
        offset = tableView?.contentOffset
    }

    func setParallax() {
        parallaxView = UIView.fromNib()
        parallaxView?.button?.addTarget(self, action: #selector(showImage(_:)), for: .touchUpInside)
        gnomeViewModel?.set(view: parallaxView)
        tableView?.parallaxHeader.view = parallaxView.contentView
        tableView?.parallaxHeader.height = 350
        tableView?.parallaxHeader.mode = .fill
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 13
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch statusBarOrientation?.rawValue {
        case 3, 4:  return 50
        default:
            let minYPosition = (navigationController?.navigationBar.frame.minY ?? 0) + CGFloat(44)
            let maxYPosition = navigationController?.tabBarController?.tabBar.frame.minY ?? 0
            return (maxYPosition - minYPosition)/CGFloat(13)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier1)
        gnomeViewModel?.set(view: cell as? DetailAttributeTableViewCell, indexPath: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:  (mainCoordinator as? NavigationCoordinator)?.push(
            type: ProfessionsDetailTableViewController.self,
            storyboardName: nil
        )
        case 3:  (mainCoordinator as? NavigationCoordinator)?.push(
            type: FriendsDetailTableViewController.self,
            storyboardName: nil
        )
        default: break
        }
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let isActiveViewer = isActiveViewer, isActiveViewer, let button = parallaxView?.button else { return }
        showImage(button)
    }

    @objc func showImage(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.3, animations: {
//            guard let isActiveViewer = self.isActiveViewer,
//            let parallaxHeader = self.tableView?.parallaxHeader,
//            let parallaxView = self.parallaxView else { return }
//            if isActiveViewer {
//                parallaxHeader.minimumHeight = self.navigationController?.navigationBar.frame.maxY ?? 0
//                parallaxHeader.height = 350
//                parallaxView.imageViewFull?.contentMode = .scaleAspectFill
//                parallaxView.imageViewBg?.contentMode = .scaleAspectFill
//                self.tabBarController?.tabBar.alpha = 1
//            } else {
//                parallaxHeader.minimumHeight = self.navigationController?.navigationBar.frame.maxY ?? 0
//                parallaxHeader.height = (self.tabBarController?.tabBar.frame.minY ?? 0)
//                parallaxView.imageViewFull?.contentMode = .scaleAspectFit
//                parallaxView.imageViewBg?.contentMode = .scaleAspectFit
//                self.tabBarController?.tabBar.alpha = 0
//            }
//        }, completion: { [unowned self] _ in
//            self.isActiveViewer = !self.isActiveViewer
//        })
    }

}
