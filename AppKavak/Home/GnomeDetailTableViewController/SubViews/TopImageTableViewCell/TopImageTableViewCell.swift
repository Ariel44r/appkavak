//
//  TopImageTableViewCell.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class TopImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewBg: UIImageView!
    @IBOutlet weak var fxView: UIVisualEffectView!
    @IBOutlet weak var imageViewFull: UIImageView!
    @IBOutlet weak var button: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
