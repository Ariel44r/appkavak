//
//  HomeCollectionViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

private let reuseIdentifier = "GnomeCollectionViewCell"

class HomeCollectionViewController: BaseCollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.collectionViewLayout = createLayoutDiffSection()
        gnomeViewModel?.fetchData()
        navigationItem.title = "Home"
        collectionView?.register(
            UINib(nibName: reuseIdentifier, bundle: nil),
            forCellWithReuseIdentifier: reuseIdentifier
        )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gnomeViewModel?.numberOfItemsInHome(with: section) ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier,
            for: indexPath
            ) as? GnomeCollectionViewCell else { fatalError("") }
        gnomeViewModel?.set(view: cell, indexPath: indexPath)
        if indexPath.section == 0 {
            (0 ..< (cell.buttons?.count ?? 0)).forEach {
                cell.buttons?[$0].tag = $0
                cell.buttons?[$0].indexPath = indexPath
                cell.buttons?[$0].addTarget(self, action: #selector(selector(_:)), for: .touchUpInside)
            }
        }
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        gnomeViewModel?.setCurrentGnome(at: indexPath)
        (mainCoordinator as? NavigationCoordinator)?.push(
            type: GnomeDetailTableViewController.self,
            storyboardName: nil
        )
    }

    func createLayoutDiffSection() -> UICollectionViewLayout {
        let layout = CompositionalLayout { (sectionIndex: Int, _: LayoutEnvironment) -> LayoutSection? in
            let columns: Int = {
                switch sectionIndex {
                case 0:     return 1
                case 1:     return 2
                case 2:     return 3
                default:    return 1
                }
            }()
            let itemSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let groupHeight = sectionIndex == 1 ? LayoutDimension.absolute(200) : LayoutDimension.absolute(100)
            let groupWidth = NSCollectionLayoutDimension.fractionalWidth(sectionIndex == 0 ? 1.5 : 1.0)
            let groupSize = NSCollectionLayoutSize(widthDimension: groupWidth, heightDimension: groupHeight)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: columns)

            let section = NSCollectionLayoutSection(group: group)
            return sectionIndex == 0 ? self.generateCarouselLayout() : section
        }
        return layout
    }

}

public typealias CompositionalLayout = UICollectionViewCompositionalLayout
public typealias LayoutEnvironment = NSCollectionLayoutEnvironment
public typealias LayoutSection = NSCollectionLayoutSection
public typealias LayoutDimension = NSCollectionLayoutDimension
