//
//  CategoryCollectionViewCell.swift
//  AppKavak
//
//  Created by Socio+ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewThumbnail: UIImageView!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var imageViewGender: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewThumbnail?.makeViewWith(features: [.roundedView(.full, .master)])
    }

}
