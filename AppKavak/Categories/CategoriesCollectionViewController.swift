//
//  CategoriesCollectionViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 20/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

private let reuseIdentifier = "GnomeCollectionViewCell"
private let reuseIdentifier0 = "CategoryCollectionViewCell"

class CategoriesCollectionViewController: BaseCollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Categories"
        collectionView?.collectionViewLayout = createLayoutDiffSection()
        collectionView?.register(
            UINib(nibName: reuseIdentifier, bundle: nil),
            forCellWithReuseIdentifier: reuseIdentifier
        )
        collectionView?.register(UINib(
            nibName: reuseIdentifier0, bundle: nil),
                                 forCellWithReuseIdentifier: reuseIdentifier0
        )
        tabBarController?.selectedIndex = 1
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return gnomeViewModel?.dataSource?.professionsGnomes?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gnomeViewModel?.dataSource?.professionsGnomes?[section].count ?? 0
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {

        let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: reuseIdentifier,
            for: indexPath
        ) as? HeaderView
        headerView?.label?.text = "Recomendados:"
        return headerView ?? UICollectionReusableView()
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var viewCell: UICollectionViewCell!
        switch indexPath.section {
        case 2, 5:
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: reuseIdentifier0,
                for: indexPath
            ) as? CategoryCollectionViewCell
            gnomeViewModel?.set(with: cell, indexPath: indexPath)
            viewCell = cell
        case 0, 1, 3, 4:
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: reuseIdentifier,
                for: indexPath
            ) as? GnomeCollectionViewCell
            gnomeViewModel?.set(with: cell, indexPath: indexPath)
            viewCell = cell
            if indexPath.section == 1 {
                (0 ..< (cell?.buttons?.count ?? 0)).forEach {
                    cell?.buttons?[$0].tag = $0
                    cell?.buttons?[$0].indexPath = indexPath
                    cell?.buttons?[$0].addTarget(self, action: #selector(selector(_:)), for: .touchUpInside)
                }
            }
        default:
            if indexPath.section%2 == 0 {
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: reuseIdentifier0,
                    for: indexPath
                ) as? CategoryCollectionViewCell
                gnomeViewModel?.set(with: cell, indexPath: indexPath)
                viewCell = cell
            } else {
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: reuseIdentifier,
                    for: indexPath
                ) as? GnomeCollectionViewCell
                gnomeViewModel?.set(with: cell, indexPath: indexPath)
                viewCell = cell
            }
        }
        return viewCell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        gnomeViewModel?.setCurrentGnomeFromCategories(at: indexPath)
        (mainCoordinator as? NavigationCoordinator)?.push(
            type: GnomeDetailTableViewController.self,
            storyboardName: nil
        )
    }

    func createLayoutDiffSection() -> UICollectionViewLayout {
            let layout = CompositionalLayout { (sectionIndex: Int, _: LayoutEnvironment) -> LayoutSection? in
                let columns: Int = {
                    switch sectionIndex {
                    case 0:     return 3
                    case 1:     return 3
                    case 2, 5, 7:     return 1
                    case 3:     return 2
                    case 4:     return 2
                    default:
                        return sectionIndex%2 == 0 ? 1 : 3
                    }
                }()
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalHeight(1.0)
                )
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                let groupHeight = NSCollectionLayoutDimension.absolute({
                    switch sectionIndex {
                    case 0:     return 150
                    case 1, 3:     return 0
                    case 2, 5, 7:     return 100
                    case 4:     return 300
                    default:
                        return sectionIndex%2 == 0 ? 100 : 130
                    }
                }())
                let groupWidth = NSCollectionLayoutDimension.fractionalWidth(1.0)
                let groupSize = NSCollectionLayoutSize(widthDimension: groupWidth, heightDimension: groupHeight)
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: columns)
                let section = NSCollectionLayoutSection(group: group)
                switch sectionIndex {
                case 1:
                    return self.generateCarouselLayout(itemsWidth: CGFloat(columns))
                case 3:
                    return self.generateCarouselLayout(itemsWidth: 1.3)
                case 6:
                    return self.generateCarouselLayout(itemsWidth: 1.7)
                default: return section
                }
            }
            return layout
        }

    func generateCarouselLayout(itemsWidth: CGFloat) -> NSCollectionLayoutSection {
        let fraction: CGFloat = CGFloat(1) / (itemsWidth == 0 ? 1 : itemsWidth)
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(1.0)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(fraction),
            heightDimension: .fractionalWidth(0.9)
        )
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPaging
        return section
    }

}
