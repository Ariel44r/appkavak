//
//  BaseCollectionViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

private let reuseIdentifier = "HeaderView"

class BaseCollectionViewController: UICollectionViewController, Coordinator {
    var childCoordinators: [Coordinator]!
    var mainCoordinator: Coordinator! { navigationController as? Coordinator }

    var searchButtonItem: UIBarButtonItem!
    var refreshcontrol: UIRefreshControl!
    var gnomeViewModel: GnomeViewModel! {
        (navigationController?.tabBarController as? BaseTabBarController)?.gnomeViewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "magnifyingglass"),
            style: .plain,
            target: self,
            action: #selector(search(_:))
        )
        collectionView?.register(
            UINib(nibName: "HeaderView", bundle: nil),
            forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader",
            withReuseIdentifier: reuseIdentifier
        )
        searchButtonItem.tintColor = .master
        refreshcontrol = UIRefreshControl()
        refreshcontrol?.addTarget(self, action: #selector(refreshControl(_:)), for: .valueChanged)
        gnomeViewModel?.delegates?.append(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
            self?.navigationItem.rightBarButtonItem = self?.searchButtonItem
            self?.collectionView?.refreshControl = self?.refreshcontrol
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.searchController = SearchEngine.shared.searchController
        SearchEngine.shared.resultsTableViewController?.delegate = self
        SearchEngine.shared.gnomeViewModel = gnomeViewModel
    }

    @objc func search(_ sender: UIBarButtonItem) {
        SearchEngine.shared.searchController?.searchBar.becomeFirstResponder()
    }

    @objc func refreshControl(_ sender: UIRefreshControl) {
//        gnomeViewModel?.fetchDataFromServer(completion: {
//            DispatchQueue.main.async {
//                sender.endRefreshing()
//            }
//        })
        gnomeViewModel?.fetchDataTLS(completion: {
            debugPrint("completion TLS")
        })
    }

    @objc func selector(_ sender: IndexButton) {
        gnomeViewModel?.setCurrentGnome(at: sender.indexPath)
        if sender.tag == 0 {
            (mainCoordinator as? NavigationCoordinator)?.push(
                type: ProfessionsDetailTableViewController.self,
                storyboardName: nil
            )
        } else {
            (mainCoordinator as? NavigationCoordinator)?.push(
                type: FriendsDetailTableViewController.self,
                storyboardName: nil
            )
        }
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        return cell
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setSearchItem()
    }

    func setSearchItem() {
        guard let heightSearchBar = SearchEngine.shared.searchController?.searchBar.frame.height else { return }
        let condition: Bool = heightSearchBar < 10.0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.navigationItem.rightBarButtonItem = condition ? self?.searchButtonItem : nil
        })
    }

}

// MARK: SearchResultsProtocol
extension BaseCollectionViewController: SearchResultsProtocol {
    func showDetailObject() {
        (mainCoordinator as? NavigationCoordinator)?.push(
            type: GnomeDetailTableViewController.self,
            storyboardName: nil
        )
    }
}

extension BaseCollectionViewController {
    func generateCarouselLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(1.0)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(0.9)
        )
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
//        group.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 0)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPaging
        return section
    }
}

// MARK: ViewModelProtocol
extension BaseCollectionViewController: ViewModelProtocol {
    func fetchDataSuccess() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { [weak self] in
            self?.collectionView?.reloadData()
        })
    }

    func fetchDataFailure(with error: APIError) { }

}
