//
//  BaseNavigationViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {
    var childCoordinators: [Coordinator]!
    var mainCoordinator: Coordinator! { tabBarController as? Coordinator }

    override func viewDidLoad() {
        super.viewDidLoad()
        start()
        self.navigationBar.prefersLargeTitles = true
    }

}

// MARK: NavigationCoordinator
extension BaseNavigationViewController: NavigationCoordinator {
    func start() {
        childCoordinators = viewControllers as? [Coordinator]
    }

    func push<T: UIViewController>(type: T.Type, storyboardName: String?) {
        debugPrint("push: \(type)")
        guard let viewController = self.getViewController(type, storyboardName: storyboardName) else { return }
        pushViewController(viewController, animated: true)
    }

    func pop() {
        debugPrint("pop")
    }
}

// MARK: UINavigationControllerDelegate
extension BaseNavigationViewController: UINavigationControllerDelegate {
    func navigationController(
        _ navigationController: UINavigationController,
        willShow viewController: UIViewController,
        animated: Bool) {

        debugPrint("willShow: \(viewController.self)")
    }

    func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool) {

        debugPrint("didShow: \(viewController.self)")
    }
}
