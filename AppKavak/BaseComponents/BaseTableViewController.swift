//
//  BaseTableViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController, Coordinator {
    var childCoordinators: [Coordinator]!
    var mainCoordinator: Coordinator! { navigationController as? Coordinator }
    var gnomeViewModel: GnomeViewModel! {
        (navigationController?.tabBarController as? BaseTabBarController)?.gnomeViewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        gnomeViewModel = GnomeViewModel()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

}
