//
//  ViewController.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, Coordinator {
    var childCoordinators: [Coordinator]!
    var mainCoordinator: Coordinator! { navigationController as? Coordinator }
    var gnomeViewModel: GnomeViewModel! {
        (navigationController?.tabBarController as? BaseTabBarController)?.gnomeViewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        gnomeViewModel = GnomeViewModel()
        start()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func start() { }

}
