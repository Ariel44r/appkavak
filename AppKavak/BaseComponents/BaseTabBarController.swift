//
//  MainCoordinator.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController, TabBarCoordinator {
    var childCoordinators: [Coordinator]!
    var mainCoordinator: Coordinator! { nil }
    var gnomeViewModel: GnomeViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        gnomeViewModel = GnomeViewModel()
        start()
        debugPrint("🥰 TabController, childCoordinators: [\(childCoordinators?.count ?? 0)] coordinators")
        childCoordinators?.forEach {
            debugPrint("[----> childCoordinator type: \($0.self)]")
        }
    }

    func start() {
        childCoordinators = viewControllers as? [NavigationCoordinator]
    }
}
