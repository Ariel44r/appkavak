//
//  UIColorExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 17/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

extension UIColor {
    class var randomNumber: CGFloat { .random(in: (0 ... 255)) }
    class var random: UIColor {
        UIColor(displayP3Red: randomNumber/255, green: randomNumber/255, blue: randomNumber/255, alpha: 1)
    }
    class var master: UIColor { UIColor(named: "master")! }
    class var tabBarTitles: UIColor { UIColor(named: "tabBarTitles")! }
}
