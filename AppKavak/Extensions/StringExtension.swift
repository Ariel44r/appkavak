//
//  StringExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

extension String {
    func getURLImage(completion: @escaping(Data?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            if let data = ImageCache.shared.getMyObject(for: self as NSString) {
                completion(data)
                debugPrint("🏠")
            } else {
                guard let url = URL(string: self) else { completion(nil);return }
                let urlRequest = URLRequest(url: url)
                debugPrint("🚀")
                URLSession.shared.asyncRequest(request: urlRequest, completion: { data, _, _ in
                    completion(data)
                    guard let myImageData = data else { return }
                    ImageCache.shared.cacheMy(object: myImageData, for: self)
                })
            }
        }
    }

    func prepareForFilter() -> String {
        return folding(options: .caseInsensitive, locale: .current)
            .folding(options: .regularExpression, locale: .current)
            .folding(options: .diacriticInsensitive, locale: .current)
            .folding(options: .diacriticInsensitive, locale: .current)
            .replacingOccurrences(of: ".", with: "")
    }
}

extension Sequence where Element: Equatable {

    var uniqueElements: [Element] {
        return self.reduce(into: []) {
            if !$0.contains($1) {
                $0.append($1)
            }
        }
    }

}
