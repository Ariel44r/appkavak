//
//  UIViewExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

enum ViewFeatures {
    case rounded
    case shadow
    case color(UIColor)
    case bordered(UIColor, CGFloat)
    case image(UIImage)
    case roundedView(RoundedType, UIColor)
    case topRounded, fullRounded, customRounded(UIRectCorner)
    case customRoundedCorner(UIRectCorner, _ cornerRadius: CGFloat)
    case vGradient([UIColor]), hGradient([UIColor])
}

enum RoundedType {
    case full, onlyLayer
}

@objc extension UIView {

    func setOffset(at yPosition: CGFloat) {
        UIView.animate(withDuration: 0.4, animations: {
            self.frame.origin.y = yPosition
        })
    }

    func setDayView(with color: UIColor) {
        UIView.animate(withDuration: 0.1, animations: {
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)

        }, completion: { _ in
            UIView.animate(withDuration: 0.1, animations: {
                self.transform = .identity

            }, completion: { _ in
                self.makeViewWith(features: [.color(color)])

            })
        })
    }

    func hideWithAnimation(hidden: Bool) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.isHidden = hidden
        })
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as? T
    }

    func makeViewWith(features: [ViewFeatures]?) {
        features?.forEach {
            switch $0 {
            case .rounded: layer.cornerRadius = 10; clipsToBounds = true
            case .shadow: setShadow()
            case .color(let color): backgroundColor = color
            case .bordered(let color, let borderWidth): setCornerRadius(color: color, borderWidth: borderWidth)
            case .image(let image): (self as? UIImageView)?.image = image
            case .roundedView(let roundedType, let color): setRounded(roundedType: roundedType, color: color)
            case .topRounded: setTopRounded()
            case .fullRounded: setFullRounded()
            case .customRounded(let corners): setRounded(at: corners, cornerRad: 10)
            case .customRoundedCorner(let corners, let cornerRadius): setRounded(at: corners, cornerRad: cornerRadius)
            case .vGradient(let colors): vGradient(colors: colors)
            case .hGradient(let colors): hGradient(colors: colors)
            }
        }
    }

    func setRounded(roundedType: RoundedType, color: UIColor) {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }

}

@objc extension UIView {

    func vGradient(colors: [UIColor]) {
        let gradientLayer = (layer.sublayers?.first as? CAGradientLayer) ?? CAGradientLayer()
        gradientLayer.frame.size = frame.size
        gradientLayer.colors = colors.map { $0.cgColor }
        let locations: [CGFloat] = (0 ..< colors.count).map { CGFloat($0) / CGFloat(colors.count-1) }
        gradientLayer.locations = locations as [NSNumber]
        layer.insertSublayer(gradientLayer, at: 0)

    }

    func hGradient(colors: [UIColor]) {
        let gradientLayer = (layer.sublayers?.first as? CAGradientLayer) ?? CAGradientLayer()
        gradientLayer.frame.size = frame.size
        gradientLayer.colors =  colors.map { $0.cgColor }
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)

    }

    func setRounded(at roundingCorners: UIRectCorner, cornerRad: CGFloat) {
        self.layer.mask = {
            let size = CGSize(width: cornerRad, height: cornerRad)
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: roundingCorners, cornerRadii: size)
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            return maskLayer
        }()
    }

    func setTopRounded() {
        self.setRounded(at: [.topLeft, .topRight], cornerRad: 30)

    }

    func setFullRounded() {
        self.setRounded(at: [.topRight, .topLeft, .bottomLeft, .bottomRight], cornerRad: 10)
    }

    func setShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.5

    }

    func setCornerRadius(color: UIColor, borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }

    func addActivityIndicator(bgColor: UIColor?) {
        self.addSubview({
            let childView = UIView()
            childView.backgroundColor = bgColor ?? .red
            childView.frame = self.bounds
            childView.addSubview({
                let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
                activityIndicator.color = .white
                activityIndicator.frame = self.bounds
                activityIndicator.startAnimating()
                return activityIndicator

            }())
            return childView
        }())
    }

    func gradient(colors: [UIColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = frame.size
        gradientLayer.colors = colors.map { $0.cgColor }
        var locations = [CGFloat]()
        for index in 0 ..< colors.count {
            locations.append(CGFloat(index) / CGFloat(colors.count-1))
        }
        gradientLayer.locations = locations as [NSNumber]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }

    func animateAsNotAllowed() {
        UIView.animate(withDuration: 0.1,
                       animations: {
            self.transform = CGAffineTransform(translationX: -15, y: 0)

        }, completion: { _ in
            UIView.animate(withDuration: 0.1,
                           delay: 0,
                           usingSpringWithDamping: 0.4,
                           initialSpringVelocity: 0.2,
                           options: .curveLinear,
                           animations: {
                self.transform = CGAffineTransform(translationX: 15, y: 0)

            }, completion: { _ in
                UIView.animate(withDuration: 0.4,
                               delay: 0,
                               usingSpringWithDamping: 0.4,
                               initialSpringVelocity: 0.2,
                               options: .curveLinear, animations: {
                    self.transform = CGAffineTransform(translationX: 0, y: 0)
                })
            })
        })
    }

}
