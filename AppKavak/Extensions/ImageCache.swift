//
//  ImageCache.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

class ImageCache: NSCache<NSString, NSData> {
    static let shared: ImageCache = ImageCache()
    private override init() { }

    func cacheMy(object: Data, for key: String) {
        setObject(object as NSData, forKey: key as NSString)
    }

    func getMyObject(for key: NSString) -> Data? {
        return object(forKey: key) as Data?
    }

    func getImage(for key: String) -> UIImage? {
        if let data64 = getMyObject(for: key as NSString),
        let imageData = Data(base64Encoded: data64),
        let image = UIImage(data: imageData) {
            return image

        }
        return nil
    }

    func deleteCachedObject(for key: String) {
        removeObject(forKey: key as NSString)

    }

}

extension UIImage {
    var base64: String {
        guard let data: Data = self.pngData() as Data? else { return "" }
        return data.base64EncodedString()

    }
}
