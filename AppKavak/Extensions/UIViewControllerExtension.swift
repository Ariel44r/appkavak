//
//  UIViewControllerExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 18/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import Foundation

extension UIViewController {
    var statusBarOrientation: UIInterfaceOrientation? {
        guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else {
            #if DEBUG
            fatalError("Could not obtain UIInterfaceOrientation from a valid windowScene")
            #else
            return nil
            #endif
        }
        return orientation
    }
}
