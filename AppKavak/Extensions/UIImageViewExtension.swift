//
//  UIImageViewExtension.swift
//  AppKavak
//
//  Created by ARIEL DIAZ on 19/07/20.
//  Copyright © 2020 ARIEL DIAZ. All rights reserved.
//

import UIKit

extension UIImageView {

    func getImage(from url: String) {
        DispatchQueue.global(qos: .background).async {
            if let image = ImageCache.shared.getImage(for: url) {
                DispatchQueue.main.async { [weak self] in
                    debugPrint("🏠")
                    self?.image = image
                }
            } else {
                debugPrint("🚀")
                DispatchQueue.main.async { [weak self] in
                    let activityView = UIActivityIndicatorView(style: .medium)
                    guard let url = URL(string: url) else { return }
                    self?.addSubview(activityView)
                    activityView.center = self?.center ?? .zero
                    activityView.startAnimating()
                    activityView.color = .label
                    DispatchQueue.global(qos: .background).async {
                        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, _, _ in
                            if let data = data?.resized(withPercentage: 0.6), let image = UIImage(data: data) {
//                                debugPrint("\n[-------------------------------------------------------------]")
//                                debugPrint("[IMAGE:]")
//                                debugPrint("[-------\(url.absoluteString)]")
//                                debugPrint("[size: \(CGFloat(data.count)/CGFloat(1000000)) MiB]")
//                                debugPrint("[-------------------------------------------------------------]\n")
                                DispatchQueue.main.async { [weak self] in
                                    self?.image = image
                                    activityView.stopAnimating()
                                    activityView.removeFromSuperview()
                                    let base64 = image.base64
                                    DispatchQueue.global(qos: .background).async {
                                        if let data = base64.data(using: .utf8) {
                                            ImageCache.shared.cacheMy(object: data, for: url.absoluteString)
                                        }
                                    }
                                }
                            } else {
                                DispatchQueue.main.async {
                                    activityView.stopAnimating()
                                    activityView.removeFromSuperview()

                                }
                            }
                        })
                        task.resume()
                    }
                }
            }
        }
    }

}

extension Data {
    func resized(withPercentage percentage: CGFloat) -> Data? {
        guard let image = UIImage(data: self) else { return nil }
        let canvasSize = CGSize(width: image.size.width * percentage, height: image.size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, image.scale)
        defer { UIGraphicsEndImageContext() }
        image.draw(in: CGRect(origin: .zero, size: canvasSize))
        let imageContext = UIGraphicsGetImageFromCurrentImageContext()
        return imageContext?.pngData()
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()

    }
}
